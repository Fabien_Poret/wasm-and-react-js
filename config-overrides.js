// config-overrides.js
module.exports = function override(config, env) {
    // New config, e.g. config.plugins.push...
    config.module.push({
        test: /\.(wasm)$/,
        loader: "file-loader",
        type: "javascript/auto",
      })
    return config
}