const bench = require('benchmark')

const Benchmark = () => {
    const sum = (a, b) => a + b
    const mult = (a, b) => a * b
    
    const suite = bench.Suite('sum')
    
    suite
      .add("Javascript", () => sum(5, 8))
      .add("WebAssembly", () => mult(5595865659898956, 85956556))
      // add listeners
      .on('cycle', function(event) {
        console.log(String(event.target));
      })
      .on('complete', function() {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
      })
      // run async
      .run({ 'async': true });
    
}

export default Benchmark