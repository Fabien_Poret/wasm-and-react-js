import { useEffect, useState } from 'react';

export const useWasm = () => {
  const [state, setState] = useState(null);
  useEffect(() => { 
    const fetchWasm = async () => {
        const go = new window.Go();
        const wasm = await fetch('lib.wasm');
        await WebAssembly.instantiateStreaming(wasm, go.importObject).then((result) => {
            go.run(result.instance);
            setState( result.instance);
        });
    };
    fetchWasm();
  }, []);
  return state;
}