import React from 'react'
import { useWasm } from './useWasm';

function App() {
  const instance = useWasm()
  console.log('add', instance && window?.add(50, 40))
  return (
    <>
      <h1>WASM & JS</h1>
      {instance && window?.add(50, 40)}
    </>
  );
}

export default App;
